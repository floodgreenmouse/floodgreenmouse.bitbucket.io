$(document).ready(function() {
	var allSlides = $(".slider__slide");
	$('.slider__buttons__page__all').text('0' + allSlides.length);
	var slideFlip = function () {
		let currentSlide = $(".slider__slide--current");
		let currentSlideIndex = $(".slider__slide--current").index();
		let nextSlideIndex = currentSlideIndex + 1;
		let nextSlide = $(".slider__slide").eq(nextSlideIndex);
		currentSlide.fadeOut(0).removeClass("slider__slide--current");
		$('.slider__buttons__page__current').text('0'+(nextSlideIndex+1));
		if (nextSlideIndex == $(".slider__slide:last").index() + 1) {
			$('.slider__buttons__page__current').text('01');
			$(".slider__slide")
				.eq(0)
				.fadeIn(500)
				.addClass("slider__slide--current");
		} else {
			nextSlide.fadeIn(500).addClass("slider__slide--current");
		}
	}

	$(".slider__buttons__next").click(function() {
		slideFlip();
	});
	$(".slider__buttons__prev").click(function() {
		let currentSlide = $(".slider__slide--current");
		let currentSlideIndex = $(".slider__slide--current").index();
		let prevSlideIndex = currentSlideIndex - 1;
		let prevSlide = $(".slider__slide").eq(prevSlideIndex);
		currentSlide.fadeOut(0).removeClass("slider__slide--current");
		prevSlide.fadeIn(500).addClass('slider__slide--current');
		$('.slider__buttons__page__current').text('0' + (currentSlideIndex));
		if(currentSlideIndex == $('slider__slide:last').index() +1) {
			let allSlides = $(".slider__slide");
			$('.slider__buttons__page__current').text('0' + allSlides.length);
		}
		
	});

	$('.content__spoiler__controls').click(function(){
		$(this).parent().find('.content__spoiler__text').slideToggle(400);
	})
});
